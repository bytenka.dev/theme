import $ from "js/lib/cash-v8.1.0";

// TODO hide and show for menu
$(document).ready(() => {
    let roots = $(".site-menu-dropdown");
    roots.each((key, value) => {
        let menu = $(value);
        let menu_button = menu.children(".site-menu-dropdown-button").first();
        let menu_items = menu.children(".site-menu-dropdown-items").first();

        // Click on icon toggles
        menu_button.on("click", () => {
            if (menu_items.css("opacity") <= 0) {
                // Show
                menu_items.css("opacity", 1);
            } else {
                // Hide
                menu_items.css("opacity", null);
            }
        });

        // Transition stuff
        menu_items.on("transitionstart", () => {
            if (menu_items.css("opacity") > 0) {
                menu_items.css("visibility", "visible");
            }
        });

        menu_items.on("transitionend", () => {
            if (menu_items.css("opacity") <= 0) {
                menu_items.css("visibility", null);
            }
        });
    });
});