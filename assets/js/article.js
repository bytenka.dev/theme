import $ from "js/lib/cash-v8.1.0";

// Zoom image on click by un-hidding the corresponding overlay 
$(document).ready(() => {
    let roots = $(".article-body figure");
    roots.each((key, value) => {
        let figure = $(value);
        let figure_img = figure.children("img").first();
        let figure_overlay = figure.children(".image-overlay").first();

        // Click on small image shows big image
        figure_img.on("click", () => {
            figure_overlay.css("opacity", 1);
        });

        // Click on overlay hides it
        figure_overlay.on("click", () => {
            figure_overlay.css("opacity", null);
        });

        // Transition stuff
        figure_overlay.on("transitionstart", () => {
            if (figure_overlay.css("opacity") > 0) {
                figure_overlay.css("visibility", "visible");
            }
        });
        
        figure_overlay.on("transitionend", () => {
            if (figure_overlay.css("opacity") <= 0) {
                figure_overlay.css("visibility", null);
            }
        });

        /*
        // Click on overlay content does not propagate
        figure_overlay.find(".overlay-content").each((key, value) => {
            $(value).on("click", (event) => {
                event.stopPropagation();
            });
        });
        */
    });
});